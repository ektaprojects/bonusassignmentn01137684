﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssignmentN01137684.Cartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Carteisan Smartesian</h1>
            <label>Enter value of X: </label>
            <asp:TextBox ID="xValue" runat="server" ></asp:TextBox>
            <asp:CompareValidator runat="server" ControlToValidate="xValue" ValueToCompare="0" Operator="NotEqual" ErrorMessage="0 is not allowed."></asp:CompareValidator>
            <asp:RegularExpressionValidator ID="xRegexValidator" runat="server" ValidationExpression="^[-+]?[\d]*$"
                ControlToValidate="xValue" ErrorMessage="Must be an Interger."></asp:RegularExpressionValidator>
            <br />
            <label>Enter value of Y: </label>
            <asp:TextBox ID="yValue" runat="server" ></asp:TextBox>
            <asp:CompareValidator runat="server" ControlToValidate="yValue" ValueToCompare="0" Operator="NotEqual" ErrorMessage="0 is not allowed."></asp:CompareValidator>
            <asp:RegularExpressionValidator ID="yRegexValidator" runat="server" ValidationExpression="^[-+]?[\d]*$"
                ControlToValidate="yValue" ErrorMessage="Must be an Interger."></asp:RegularExpressionValidator>
            <br />
            <asp:Button runat="server" ID="button" Text="Submit Cordinates" OnClick="CheckQuadrant" />

            <div runat="server" id="quadrantResult">

            </div>

        </div>
    </form>
</body>
</html>
