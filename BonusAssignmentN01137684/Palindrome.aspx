﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="BonusAssignmentN01137684.Palindrome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Enter a string of characters</label>
            <asp:TextBox ID="stringForPalindrome" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="stringRegexValidator" runat="server" ValidationExpression="^[a-zA-Z ]+$"
                ControlToValidate="stringForPalindrome" ErrorMessage="Only characters and spaces."></asp:RegularExpressionValidator>
            <asp:Button ID="checkPalindrome" runat="server" Text="Check if Palindrome" OnClick="CheckString"/>
        </div>
        <div id="result" runat="server">

        </div>

    </form>
</body>
</html>
