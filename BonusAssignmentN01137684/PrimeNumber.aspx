﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrimeNumber.aspx.cs" Inherits="BonusAssignmentN01137684.PrimeNumber" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Enter an Integer</label>
            <asp:TextBox runat="server" ID="intrgerTobeChecked"></asp:TextBox>
            <asp:RegularExpressionValidator ID="intRegexValidator" runat="server" ValidationExpression="^[-+]?[\d]*$"
                ControlToValidate="intrgerTobeChecked" ErrorMessage="Must be an Interger."></asp:RegularExpressionValidator>
            <asp:Button runat="server" ID="checkPrime" Text="Check if Prime" OnClick="CheckPrime" />
        </div>
        <div runat="server" id="result">

        </div>
    </form>
</body>
</html>
