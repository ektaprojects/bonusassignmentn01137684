﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonusAssignmentN01137684
{
    public class CartesianPlane
    {
        public int inputX;
        public int inputY;

        public CartesianPlane(int x, int y)
        {
            inputX = x;
            inputY = y;
        }
    }
}