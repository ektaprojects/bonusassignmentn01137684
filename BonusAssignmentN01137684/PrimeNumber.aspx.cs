﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignmentN01137684
{
    public partial class PrimeNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CheckPrime(object sender, EventArgs e)
        {
            int checkflag = 0;
            if (!Page.IsValid)
            {
                return;
            }
            int number = int.Parse(intrgerTobeChecked.Text);
            Divisible checkNumber = new Divisible(number);

            for(int i = 2; i<= checkNumber.num/ 2; i++)
            {
                if(checkNumber.num % i == 0)
                {
                    result.InnerHtml = checkNumber.num + " is not Prime.";
                    checkflag = 1;
                    break;
                }
            }
            if(checkflag == 0)
            {
                result.InnerHtml = checkNumber.num + " is Prime.";
            }

        }
    }
}