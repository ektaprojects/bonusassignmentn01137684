﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignmentN01137684
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CheckQuadrant(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            int xx = int.Parse(xValue.Text);
            int yy = int.Parse(yValue.Text);

            CartesianPlane newcartesian = new CartesianPlane(xx,yy);

            if(newcartesian.inputX > 0 && newcartesian.inputY > 0)
            {
                quadrantResult.InnerHtml = "(" + newcartesian.inputX + "," + newcartesian.inputY + ")" + " lies in first quadrant.";
            }
            else if (newcartesian.inputX < 0 && newcartesian.inputY > 0)
            {
                quadrantResult.InnerHtml = "(" + newcartesian.inputX + "," + newcartesian.inputY + ")" + " lies in Second quadrant.";
            }
            else if (newcartesian.inputX < 0 && newcartesian.inputY < 0)
            {
                quadrantResult.InnerHtml = "(" + newcartesian.inputX + "," + newcartesian.inputY + ")" + " lies in third quadrant.";
            }
            else
            {
                quadrantResult.InnerHtml = "(" + newcartesian.inputX + "," + newcartesian.inputY + ")" + " lies in forth quadrant.";
            }

        }
    }
}