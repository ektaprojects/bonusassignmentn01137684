﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignmentN01137684
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CheckString(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            String stringToCheck = stringForPalindrome.Text.ToString();
            String newString = stringToCheck.ToLower().Replace(" ", string.Empty);
            int length = newString.Length;
            String reverseString = "";
            StringBling stringObject = new StringBling(newString);

            for(int i= length-1; i>=0; i--)
            {
                reverseString = reverseString + stringObject.str[i];
            }
            if (reverseString.Equals(stringObject.str))
            {
                result.InnerHtml = "String is Palindrome";
            }
            else
            {
                result.InnerHtml = "String is not Palindrome";
            }

        }
    }
}